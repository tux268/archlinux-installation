# Arch Linux Installation Guide, forked from https://github.com/nhedger/archlinux-installation
Btrfs part is courtesy of @zorvalt
 
This guide will help you install Arch Linux in EFI mode, within a fully encrypted LVM. Please keep in mind that this guide may contain instructions that are specific to my setup. Always understand what you are doing, DO NOT simply copy/paste.

* EFI
* LVM2 on LUKS
* systemd-boot

## Table of Contents

1. [Before anything else](#before-anything-else)
2. [Configuring the disk](#configuring-the-disk)
3. [Installing the base system](#installing-the-base-system)
4. [Adding relevant hooks to mkinitcpio.conf](#adding-relevant-hooks-to-mkinitcpioconf)
5. [Installing the Bootloader](#installing-the-bootloader)
6. [Finishing the Installation](#finishing-the-installation)

## Before anything else
```shell
# Set the keyboard layout
loadkeys fr_CH-latin1

# (optional) Load a bigger font if you have a HiDPI monitor
setfont latarcyrheb-sun32

# Make sure you are connected to the Internet
ping archlinux.org

# Synchronize the system clock with the network
timedatectl set-ntp true
```

## Configuring the disk

### Partitioning the disk
#### Ext4 version
<table>
<tr>
  <td rowspan="2">
    <strong>EFI system partition</strong> <br>
    <code>/boot</code> <br>
    <code>/dev/nvme0nXp1</code>
  </td>
</tr>
  <tr>
    <td colspan="3">
      <strong>LUKS2 encrypted partition</strong> <br>
      <code>/dev/mapper/luks</code> <br>
      <code>/dev/nvme0nXp2</code>
      <hr>
      <table>
        <tr>
          <td>
            <strong>swap partition</strong> <br>
            <code>swap</code> <br>
            <code>/dev/mapper/arch-swap</code>
          </td>
          <td>
            <strong>root partition</strong> <br>
            <code>/boot</code> <br>
            <code>/dev/mapper/arch-root</code>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

#### Btrfs version

<table>
	<tbody>
		<tr>
			<td rowspan="2"><strong>EFI system partition</strong> <br /> <code>/boot</code> <br /> <code>/dev/nvme0nXp1</code></td>
		</tr>
		<tr>
			<td colspan="2"><strong>LUKS2 encrypted partition</strong> <br /> <code>/dev/mapper/luks</code> <br /> <code>/dev/nvme0nXp2</code><hr />
				<table>
					<tbody>
						<tr>
							<td><strong>swap partition</strong> <br /> <code>swap</code> <br /> <code>/dev/mapper/arch-swap</code></td>
							<td><strong>root partition</strong> <br /> <code>/boot</code> <br /> <code>/dev/mapper/arch-root</code><br /> <code>@</code>
								<table>
									<tbody>
										<tr>
											<td><strong>@root</strong> <br /> <code>/</code></td>
											<td><strong>@home</strong> <br /> <code>/home</code></td>
											<td><strong>@var</strong> <br /> <code>/var</code></td>
											<td><strong>@pkg_cache</strong> <br /> <code>/var/cache/pacman/pkg</code></td>
											<td><strong>...</strong> <br /> <code>...</code></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>

```shell
# Create a new GUID partition table
parted -s /dev/nvme0nX mklabel gpt

# Create the EFI system partition and set its boot flag
parted -s /dev/nvme0nX mkpart primary 1 513
parted -s /dev/nvme0nX set 1 boot on

# Create the LUKS partition
parted -s /dev/nvme0nX mkpart primary 513 100%
cryptsetup luksFormat --type luks2 /dev/nvme0nXp2
```

### Creating the logical volumes
```shell
# Open the encrypted LUKS partition
cryptsetup open /dev/nvme0nXp2 luks

# Create a physical volume
pvcreate /dev/mapper/luks

# Create de new volume group
vgcreate arch /dev/mapper/luks

# Create the logical swap partition of xx GB
lvcreate -L xxG arch -n swap

# Create the logical root partition (will take up the remaining free space)
lvcreate -l 100%FREE arch -n root
```

### Formating the partitions
```shell
# Format the boot partition
mkfs.vfat -F32 /dev/nvme0nXp1

# Format the logical volumes
mkswap /dev/mapper/arch-swap 

# ext4 variant 
mkfs.ext4 /dev/mapper/arch-root


# btrfs variant
mkfs.btrfs --nodiscard --label ARCH --csum xxhash /dev/mapper/arch-root  
```
### Btrfs subvolumes creation (Only for the btrfs variant)
| Subvolume  | Mount point           |
| ---------- | --------------------- |
| @root      | /                     |
| @home      | /home                 |
| @snapshots | /.snapshots           |
| @var       | /var                  |
| @var_log   | /var/log              |
| @pkg_cache | /var/cache/pacman/pkg |
| @opt       | /opt                  |

```shell

# Mount the root partition btrfs
opts_btrfs=defaults,noatime,nodiratime,ssd,compress=zstd
mount -o $opts_btrfs /dev/mapper/arch-root /mnt

cd /mnt

btrfs subvolume create @
btrfs subvolume create @/home
btrfs subvolume create @/.snapshots

# Disabling COW on /var (recomended for big files)
btrfs subvolume create @/var
chattr +C /mnt/@/var

btrfs subvolume create @/var/log

mkdir -p @/var/cache/pacman
btrfs subvolume create @/var/cache/pacman/pkg

btrfs subvolume create @/opt


cd /
umount -R /mnt
```

### Mounting the file system
#### Ext4 variant
```shell
# Mount the root partition ext4 version
mount /dev/mapper/arch-root /mnt

# Mount the boot partition
mkdir /mnt/boot
mount /dev/nvme0nXp1 /mnt/boot

# Set the swap partition
swapon /dev/mapper/arch-swap
```
#### Btrfs variant

```shell
mount -o $opts_btrfs,subvol=@ /dev/mapper/arch-root /mnt
mount -o $opts_btrfs,subvol=@/home /dev/mapper/arch-root /mnt/home
mount -o $opts_btrfs,subvol=@/.snapshots /dev/mapper/arch-root /mnt/.snapshots
mount -o $opts_btrfs,subvol=@/var /dev/mapper/arch-root /mnt/var
mount -o $opts_btrfs,subvol=@/var/log /dev/mapper/arch-root /mnt/var/log
mount -o $opts_btrfs,subvol=@/var/cache/pacman/pkg /dev/mapper/arch-root /mnt/var/cache/pacman/pkg
mount -o $opts_btrfs,subvol=@/opt /dev/mapper/arch-root /mnt/opt

mkdir /mnt/boot
mount /dev/nvme0n1p1 /mnt/boot
swapon /dev/mapper/arch-swap
```

## Installing the base system
### Updating the mirrorlist
This step is optional.

```shell
# Move the original mirrorlist
mv /etc/pacman.d/mirrorlist{,.orig}

# Generate a new mirrorlist ordered by connection speed
reflector --latest 100 --protocol http --protocol https --sort rate --save /etc/pacman.d/mirrorlist

# Update pacman
pacman -Syy
```

### Installing the base packages
```shell
pacstrap /mnt base base-devel linux linux-firmware lvm2 btrfs-progs vim sudo
```


### Generating the file systems table (fstab)
```shell
# Export the fstab
genfstab -U /mnt >> /mnt/etc/fstab
```

### Chrooting into the new system
```shell
# Change root into the new system
arch-chroot /mnt
```

### Setting the time zone
```shell
# Set the time zone
ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# Synchronize the clock
hwclock --systohc
```

### Setting up locale and persist keyboard layout
```shell
# Uncomment fr_CH.UTF-8 and en_US.UTF-8 in locale.gen file
# Repeat for any locale to add
sed -i '/fr_CH.UTF-8/s/^#//g' /etc/locale.gen
sed -i '/en_US.UTF-8/s/^#//g' /etc/locale.gen

# Generate locale
locale-gen

# Set the LANG variable
echo LANG=en_US.UTF-8 >> /etc/locale.conf

#Make the keyboard layout persistent
echo KEYMAP=fr_CH-latin1 >> /etc/vconsole.conf
```

### Setting up hostname and hosts file
```shell
# Set the hostname
echo myhostname > /etc/hostname

# Setup hosts file
cat << EOF >> /etc/hosts
127.0.0.1    localhost
::1          localhost
127.0.0.1    myhostname.localdomain  myhostname
EOF
```

### Installing other packages you might need
For example a network manager if you don't want to look like a fucking IDIOT later!

```shell
pacman -S The_Fucking_World

# Intel ucode for intel procs
pacman -S intel-ucode

# or AMD ucode for AMD procs

pacman -S amd-ucode
```
### SSD specific

Reduce the swapiness. Use the value '0' to completely disable it.

```sh
echo "vm.swappiness=10" >> /etc/sysctl.d/99-sysctl.conf
```

Periodic TRIM

```sh
pacman -S hdparm util-linux
systemctl enable fstrim.timer
```


## Adding relevant hooks to mkinitcpio.conf

Add the `keyboard`, `systemd`, `sd-vconsole`, `sd-encrypt` and `sd-lvm2` hooks to `/etc/mkinitcpio.conf` as well as the btrfs tools for the generation of the initramfs.

```
BINARIES=(btrfs)
[...]
HOOKS=(base systemd autodetect modconf block keyboard sd-vconsole sd-encrypt sd-lvm2 fsck filesystems)
```

## Installing the Bootloader

```shell
# Install the bootloader to the ESP
bootctl --path=/boot install

# Create bootloader loader configuration
cat << EOF > /boot/loader/loader.conf
default   arch
timeout   2
editor    0
EOF

# Add boot entry ext4 version (adapt ucode to your CPU)
cat << EOF > /boot/loader/entries/arch.conf
title   Arch Linux
linux   /vmlinuz-linux
initrd  /<CPU>-ucode.img
initrd  /initramfs-linux.img
options rd.luks.name=<UUID-PLACEHOLDER>=cryptlvm resume=/dev/mapper/arch-swap root=/dev/mapper/arch-root
EOF


# Add boot entry btrfs version (adapt ucode to your CPU)
cat << EOF > /boot/loader/entries/arch.conf
title   Arch Linux
linux   /vmlinuz-linux
initrd  /<CPU>-ucode.img
initrd  /initramfs-linux.img
options rd.luks.uuid=<UUID-PLACEHOLDER> rd.luks.options=discard root=/dev/mapper/arch-root rootflags=subvol=@ rw
EOF

# Replace the placeholder with the encrypted partition's UUID
sed -i "s/<UUID-PLACEHOLDER>/$(blkid -s UUID -o value /dev/nvme0nXp2)/g" /boot/loader/entries/arch.conf

# Replace the CPU placeholder with the actual cpu vendor (either amd or intel)
sed -i "s/<CPU>/amd/g" /boot/loader/entries/arch.conf
```

## Finishing the Installation
```shell
# Generate new initramfs
mkinitcpio -p linux

# Add a root password
passwd

# Exit the chroot environment
exit

# Unmount everything
umount -R /mnt
swapoff /dev/mapper/arch-swap

# Reboot
reboot
```
